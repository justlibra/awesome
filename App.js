/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import Hero from './component/Hero';
import Home from './component/Home';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {Provider} from 'react-redux';
import redux from './redux/redux';
import {WebViewComponent} from './component/WebView';

const RootStack = createStackNavigator(
  {
    Home: Home,
    Hero: Hero,
    WebViewComponent: WebViewComponent,
  },
  {
    initialRouteName: 'Home',
  },
);

const AppContainer = createAppContainer(RootStack);

export class App extends React.Component {
  render() {
    return (
      <Provider store={redux}>
        <AppContainer />
      </Provider>
    );
  }
}
