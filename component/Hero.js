import React from 'react';
import {View, Text} from 'react-native';
import {Item, Input, Button, Content} from 'native-base';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';

const validate = values => {
  const error = {};
  error.name = '';
  if (!values.name) {
    error.name = 'This field is require.';
    // } else if (!/[A-Za-z\s]*/.match(values.name)) {
  } else if (
    values.name.match(/[A-Za-z\s]*/) &&
    values.name.match(/[A-Za-z\s]*/)[0] !==
      values.name.match(/[A-Za-z\s]*/).input
  ) {
    error.name = 'Name only contains letters and space character.';
  }

  return error;
};

const renderInput = ({input, label, type, meta: {touched, error, warning}}) => {
  var hasError = false;
  if (error !== undefined) {
    hasError = true;
  }
  return (
    <Item error={hasError}>
      <Input {...input} />
      {hasError ? <Text>{error}</Text> : <Text />}
    </Item>
  );
};

class Hero extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.props.navigation.state.params) {
      this.props.reduxInitValue(this.props.navigation.state.params.item);
    }
  }

  handleClick() {
    if (this.props.valid) {
      if (this.props.labelButton === 'Add') {
        this.props.reduxAddHero(this.props.heroForm.values.name);
        this.props.navigation.navigate('Home');
      } else {
        this.props.reduxDeleteHero(this.props.heroForm.values.id);
        this.props.navigation.navigate('Home');
      }
    }
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          alignItems: 'stretch',
        }}>
        <View>
          <Text>Name: </Text>
        </View>
        <Content padder>
          <Field name="name" component={renderInput} />
          <Button
            block
            primary
            onPress={() => this.handleClick()}
            disabled={!this.props.valid}>
            <Text>{this.props.labelButton}</Text>
          </Button>
        </Content>
      </View>
    );
  }
}

const mapStateToProps = state => {
  // Redux Store --> Component
  return {
    ...state.form,
    initialValues: !state.todo.initValue ? {name: ''} : state.todo.initValue,
    valid: !state.todo.initValue ? false : true,
    labelButton:
      state.form.heroForm &&
      state.form.heroForm.values &&
      (!state.form.heroForm.values.id || state.form.heroForm.values.id === '')
        ? 'Add'
        : 'Delete',
  };
};

// Map Dispatch To Props (Dispatch Actions To Reducers. Reducers Then Modify The Data And Assign It To Your Props)
const mapDispatchToProps = dispatch => {
  // Action
  return {
    reduxAddHero: name => {
      dispatch({
        type: 'ADD_HERO_ASYNC',
        value: name,
      });
    },
    reduxDeleteHero: id => {
      dispatch({
        type: 'DELETE_HERO_ASYNC',
        value: id,
      });
    },
    reduxInitValue: item => {
      dispatch({
        type: 'INIT_VALUE',
        value: item,
      });
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(
  reduxForm({
    form: 'heroForm',
    validate,
    destroyOnUnmount: true,
    enableReinitialize: true,
  })(Hero),
);
