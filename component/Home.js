import React from 'react';
import {Text, View, FlatList, StyleSheet, Button} from 'react-native';
import {connect} from 'react-redux';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});

class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.reduxGetHero();
  }

  render() {
    return (
      <View style={styles.container}>
        {
          <FlatList
            data={this.props.dataSource}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => (
              <Text
                onPress={() =>
                  this.props.navigation.navigate('Hero', {item: item})
                }
                key={item.id}
                style={styles.item}>
                {item.name}
              </Text>
            )}
            extraData={this.props}
          />
        }

        <Button
          title="Add Hero"
          onPress={() => this.props.navigation.navigate('Hero', {item: {}})}
        />
        <Button
          title="View"
          onPress={() => this.props.navigation.navigate('WebViewComponent')}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  // Redux Store --> Component
  return {
    dataSource: state.todo.dataSource,
  };
};

// Map Dispatch To Props (Dispatch Actions To Reducers. Reducers Then Modify The Data And Assign It To Your Props)
const mapDispatchToProps = dispatch => {
  // Action
  return {
    reduxGetHero: () =>
      dispatch({
        type: 'GET_HERO_ASYNC',
      }),
  };
};

// Exports
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
