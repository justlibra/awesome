import React from 'react';
import {View, Text, Button} from 'react-native';

export class TestViewComponent extends React.Component {
  constructor(props) {
    super(props);
    console.log('message');
  }

  onAlert() {
    alert('Click button');
  }

  render() {
    return (
      <View>
        <Text>Hello World</Text>
        <Button onClick={() => this.onAlert()}>
          <Text>Click</Text>
        </Button>
      </View>
    );
  }
}
