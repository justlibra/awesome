import React from 'react';
import {WebView} from 'react-native-webview';

export class WebViewComponent extends React.Component {
  render() {
    return (
      <WebView
        originWhitelist={['*']}
        style={{marginTop: 20}}
        injectedJavaScript={require('./TestView.js')}
        source={{html: '<div>Hello world</div>'}}
        javaScriptEnabled={true}
      />
    );
  }
}
