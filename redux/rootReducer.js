import {combineReducers} from 'redux';
import todoReducer from './todoReducer';
import {reducer as formReducer} from 'redux-form';

const reducers = {
  form: formReducer,
  todo: todoReducer,
};

const rootReducer = combineReducers(reducers);

export default rootReducer;
