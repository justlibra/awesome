import {fork, all} from 'redux-saga/effects';
import {watchAddHero} from './todoSaga';
import {watchGetHero} from './todoSaga';
import {watchDeleteHero} from './todoSaga';

const sagas = function*() {
  yield all([fork(watchAddHero)]);
  yield all([fork(watchGetHero)]);
  yield all([fork(watchDeleteHero)]);
};
export default sagas;
