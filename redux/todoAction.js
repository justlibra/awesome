export const addSuccess = payload => ({
  type: 'ADD_SUCCESS',
  payload,
});

export const addFailure = payload => ({
  type: 'ADD_FAILURE',
  payload,
});

export const handleHero = payload => ({
  type: 'HANDLE_HERO',
  payload,
});

export default {
  addSuccess: addSuccess,
  addFailure: addFailure,
  handleHero: handleHero,
};
