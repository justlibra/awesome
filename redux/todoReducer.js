const initData = {
  dataSource: [],
};

const todoReducer = (state = initData, action) => {
  switch (action.type) {
    case 'HANDLE_HERO':
      return {
        ...state,
        dataSource: action.dataSource,
      };
    case 'ADD_HERO':
      var stateResult = state.dataSource;
      var id = stateResult.sort(item => +item.id).reverse();
      stateResult.push({id: (+id[0].id + 1).toString(), name: action.name});
      return {
        ...state,
        dataSource: [...stateResult],
      };
    case 'ADD_FAILURE':
      return {
        ...state,
      };
    case 'INIT_VALUE':
      return {
        ...state,
        initValue: action.value,
      };
    case 'DELETE_HERO':
      var stateResult = state.dataSource;
      var index = stateResult.findIndex(item => +item.id === +action.id);
      if (index !== -1) {
        stateResult.splice(index, 1);
      }
      return {
        ...state,
        dataSource: [...stateResult],
      };
    default:
      return state;
  }
};
export default todoReducer;
