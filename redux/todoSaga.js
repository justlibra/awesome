import {delay, takeEvery, put} from 'redux-saga/effects';

function* addHero(action) {
  try {
    yield delay(1000);
    // Dispatch Action To Redux Store
    yield put({
      type: 'ADD_HERO',
      name: action.value,
    });
  } catch (error) {
    // CHANGE LATER
    console.log(error);
  }
}

// Generator: Watch Increase Counter
export function* watchAddHero() {
  yield takeEvery('ADD_HERO_ASYNC', addHero);
}

function* deleteHero(action) {
  try {
    yield delay(1000);
    // Dispatch Action To Redux Store
    yield put({
      type: 'DELETE_HERO',
      id: action.value,
    });
  } catch (error) {
    // CHANGE LATER
    console.log(error);
  }
}

// Generator: Watch Increase Counter
export function* watchDeleteHero() {
  yield takeEvery('DELETE_HERO_ASYNC', deleteHero);
}

function* getHero() {
  try {
    yield delay(1000);
    // Dispatch Action To Redux Store
    yield put({
      type: 'HANDLE_HERO',
      dataSource: [
        {id: '1', name: 'Iron man'},
        {id: '2', name: 'Cap'},
        {id: '3', name: 'Black Widow'},
        {id: '4', name: 'Spider man'},
        {id: '5', name: 'Thor'},
      ],
    });
  } catch (error) {
    // CHANGE LATER
    console.log(error);
  }
}

// Generator: Watch Increase Counter
export function* watchGetHero() {
  yield takeEvery('GET_HERO_ASYNC', getHero);
}
